
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.
    Authors(from Quasar team): Piotr Nikiel
        
    This file is part of Quasar.
   
    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
    
    This file was completely generated by Quasar (additional info: using transform designToConfigValidator.xslt) 
    on 2020-02-11T19:10:14.254-05:00
 */


#include <iostream>

#include <ASUtils.h>
#include <ASInformationModel.h>

#include <Utils.h>

#include <DRoot.h>

#include <Configurator.h>
#include <Configuration.hxx>






#include "ASTSerialProxy.h"

#include "DTSerialProxy.h"

#include "ASTTi.h"

#include "DTTi.h"





using namespace std;

	// HEADERS OF VALIDATOR


bool validateTSerialProxy (Device::DTSerialProxy * object);

bool validateTTi (Device::DTTi * object);


	// BODIES OF VALIDATOR

bool
validateTSerialProxy (Device::DTSerialProxy * object)
{

  return true;
}

bool
validateTTi (Device::DTTi * object)
{

  return true;
}


	// ROOT VALIDATOR


bool
validateRootHasTSerialProxy (Device::DRoot * object)
{


  size_t size = object->tserialproxys ().size ();
  (void) size;			/* prevent unused variable warning if bounds are not given in design file */


for (Device::DTSerialProxy * child:object->tserialproxys ())
    {
      validateTSerialProxy (child);
    }

  return true;
}

bool
validateRootHasTTi (Device::DRoot * object)
{


  size_t size = object->ttis ().size ();
  (void) size;			/* prevent unused variable warning if bounds are not given in design file */


for (Device::DTTi * child:object->ttis ())
    {
      validateTTi (child);
    }

  return true;
}


bool
validateRoot (Device::DRoot * root)
{

  validateRootHasTSerialProxy (root);

  validateRootHasTTi (root);

  return true;			/* it is assumed that improper config would throw */
}





bool
validateDeviceTree ()
{
  Device::DRoot * deviceRoot = Device::DRoot::getInstance ();
  return validateRoot (deviceRoot);

}
