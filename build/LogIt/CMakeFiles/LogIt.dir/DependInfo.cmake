# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/grosin/tmp/opcua-server/LogIt/src/ComponentAttributes.cpp" "/home/grosin/tmp/opcua-server/build/LogIt/CMakeFiles/LogIt.dir/src/ComponentAttributes.cpp.o"
  "/home/grosin/tmp/opcua-server/LogIt/src/LogIt.cpp" "/home/grosin/tmp/opcua-server/build/LogIt/CMakeFiles/LogIt.dir/src/LogIt.cpp.o"
  "/home/grosin/tmp/opcua-server/LogIt/src/LogItInstance.cpp" "/home/grosin/tmp/opcua-server/build/LogIt/CMakeFiles/LogIt.dir/src/LogItInstance.cpp.o"
  "/home/grosin/tmp/opcua-server/LogIt/src/LogLevels.cpp" "/home/grosin/tmp/opcua-server/build/LogIt/CMakeFiles/LogIt.dir/src/LogLevels.cpp.o"
  "/home/grosin/tmp/opcua-server/LogIt/src/LogRecord.cpp" "/home/grosin/tmp/opcua-server/build/LogIt/CMakeFiles/LogIt.dir/src/LogRecord.cpp.o"
  "/home/grosin/tmp/opcua-server/LogIt/src/LogSinks.cpp" "/home/grosin/tmp/opcua-server/build/LogIt/CMakeFiles/LogIt.dir/src/LogSinks.cpp.o"
  "/home/grosin/tmp/opcua-server/LogIt/src/StdOutLog.cpp" "/home/grosin/tmp/opcua-server/build/LogIt/CMakeFiles/LogIt.dir/src/StdOutLog.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BACKEND_OPEN62541"
  "LOGIT_BACKEND_STDOUTLOG"
  "SUPPORT_XML_CONFIG"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "open62541-compat/extern/open62541/include"
  "Device/generated"
  "../Configuration"
  "Configuration"
  "../AddressSpace/include"
  "AddressSpace/include"
  "../Configuration/include"
  "Configuration/include"
  "../Common/include"
  "Common/include"
  "../Server/include"
  "Server/include"
  "../Device/include"
  "Device/include"
  "../Meta/include"
  "Meta/include"
  "../LogIt/include"
  "LogIt/include"
  "../CalculatedVariables/include"
  "CalculatedVariables/include"
  "../open62541-compat/include"
  "open62541-compat/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
