# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/grosin/tmp/opcua-server/CalculatedVariables/ext_components/muparser-amalgamated/src/muParser.cpp" "/home/grosin/tmp/opcua-server/build/CalculatedVariables/CMakeFiles/CalculatedVariables.dir/ext_components/muparser-amalgamated/src/muParser.cpp.o"
  "/home/grosin/tmp/opcua-server/CalculatedVariables/src/CalculatedVariable.cpp" "/home/grosin/tmp/opcua-server/build/CalculatedVariables/CMakeFiles/CalculatedVariables.dir/src/CalculatedVariable.cpp.o"
  "/home/grosin/tmp/opcua-server/CalculatedVariables/src/CalculatedVariablesChangeListener.cpp" "/home/grosin/tmp/opcua-server/build/CalculatedVariables/CMakeFiles/CalculatedVariables.dir/src/CalculatedVariablesChangeListener.cpp.o"
  "/home/grosin/tmp/opcua-server/CalculatedVariables/src/CalculatedVariablesEngine.cpp" "/home/grosin/tmp/opcua-server/build/CalculatedVariables/CMakeFiles/CalculatedVariables.dir/src/CalculatedVariablesEngine.cpp.o"
  "/home/grosin/tmp/opcua-server/CalculatedVariables/src/ParserVariable.cpp" "/home/grosin/tmp/opcua-server/build/CalculatedVariables/CMakeFiles/CalculatedVariables.dir/src/ParserVariable.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BACKEND_OPEN62541"
  "MUPARSER_STATIC"
  "SUPPORT_XML_CONFIG"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "open62541-compat/extern/open62541/include"
  "Device/generated"
  "../Configuration"
  "Configuration"
  "../AddressSpace/include"
  "AddressSpace/include"
  "../Configuration/include"
  "Configuration/include"
  "../Common/include"
  "Common/include"
  "../Server/include"
  "Server/include"
  "../Device/include"
  "Device/include"
  "../Meta/include"
  "Meta/include"
  "../LogIt/include"
  "LogIt/include"
  "../CalculatedVariables/include"
  "CalculatedVariables/include"
  "../open62541-compat/include"
  "open62541-compat/include"
  "../CalculatedVariables/ext_components/muparser-amalgamated/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
