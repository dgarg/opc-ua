# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/grosin/tmp/opcua-server/Server/src/BaseQuasarServer.cpp" "/home/grosin/tmp/opcua-server/build/Server/CMakeFiles/Server.dir/src/BaseQuasarServer.cpp.o"
  "/home/grosin/tmp/opcua-server/Server/src/QuasarServer.cpp" "/home/grosin/tmp/opcua-server/build/Server/CMakeFiles/Server.dir/src/QuasarServer.cpp.o"
  "/home/grosin/tmp/opcua-server/Server/src/QuasarServerCallback.cpp" "/home/grosin/tmp/opcua-server/build/Server/CMakeFiles/Server.dir/src/QuasarServerCallback.cpp.o"
  "/home/grosin/tmp/opcua-server/Server/src/main.cpp" "/home/grosin/tmp/opcua-server/build/Server/CMakeFiles/Server.dir/src/main.cpp.o"
  "/home/grosin/tmp/opcua-server/Server/src/opcserver.cpp" "/home/grosin/tmp/opcua-server/build/Server/CMakeFiles/Server.dir/src/opcserver.cpp.o"
  "/home/grosin/tmp/opcua-server/Server/src/opcserver_open62541.cpp" "/home/grosin/tmp/opcua-server/build/Server/CMakeFiles/Server.dir/src/opcserver_open62541.cpp.o"
  "/home/grosin/tmp/opcua-server/Server/src/serverconfigxml.cpp" "/home/grosin/tmp/opcua-server/build/Server/CMakeFiles/Server.dir/src/serverconfigxml.cpp.o"
  "/home/grosin/tmp/opcua-server/Server/src/shutdown.cpp" "/home/grosin/tmp/opcua-server/build/Server/CMakeFiles/Server.dir/src/shutdown.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BACKEND_OPEN62541"
  "SUPPORT_XML_CONFIG"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "open62541-compat/extern/open62541/include"
  "Device/generated"
  "../Configuration"
  "Configuration"
  "../AddressSpace/include"
  "AddressSpace/include"
  "../Configuration/include"
  "Configuration/include"
  "../Common/include"
  "Common/include"
  "../Server/include"
  "Server/include"
  "../Device/include"
  "Device/include"
  "../Meta/include"
  "Meta/include"
  "../LogIt/include"
  "LogIt/include"
  "../CalculatedVariables/include"
  "CalculatedVariables/include"
  "../open62541-compat/include"
  "open62541-compat/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
